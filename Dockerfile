FROM node:18
WORKDIR /app
# Cache package.json for better builds
COPY package.json .
ARG NODE_ENV
RUN if [ "$NODE_ENV" = "development" ]; \
  then npm install; \
  else npm install --only=production; \
  fi;
COPY . ./
ENV PORT 3000
EXPOSE $PORT
CMD ["node", "index.js"]