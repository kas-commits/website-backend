# Website Backend

## Installation and Usage

The install process is very simple assuming you have docker properly setup on the host machine.

### Starting the service

To begin running this service simply run

```sh
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up --detach --build
```

### Stopping the service

To tear down the service you can run

```sh
docker-compose down --volumes
```